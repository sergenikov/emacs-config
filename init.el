;; Load my configuration files

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; Load custom configuration files.
;; There should be a file per each package installed containing configurations
;; only for that package.
(setq my-config-files-to-load
      '(
	"~/.emacs.d/basic-config.el"
	"~/.emacs.d/function-config.el"
	"~/.emacs.d/advanced-config.el"
	"~/.emacs.d/melpa-config.el"
	"~/.emacs.d/magit-config.el"
	"~/.emacs.d/key-config.el"
	"~/.emacs.d/projectile-config.el"
	"~/.emacs.d/ido-config.el"
	"~/.emacs.d/evil-config.el"
	)
      )

(dolist (config-file my-config-files-to-load)
  (let ((full-filename config-file))
	(load-file config-file)
    ))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "ef0d2cd0b5ecebd6794a2012ffa08393e536b33e3e377ac2930bf5d7304dcb21" default)))
 '(global-visual-line-mode t)
 '(package-selected-packages
   (quote
    (solarized-theme atomic-chrome ox-twbs evil evil-mode projectile magit))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

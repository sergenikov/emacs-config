;; emacs built-ins coonfiguration
;; anything third-party or custom scripts should go into advanced-config.el
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-splash-screen t)
(setq make-backup-files nil)
(global-linum-mode 0)
(display-time-mode 1)
(display-battery-mode 1)
(setq column-number-mode t)
(electric-pair-mode 1)
(show-paren-mode 1)
(savehist-mode 1)
(size-indication-mode 1)
(setq visible-bell nil)
(setq ring-bell-function 'nil)
(setq dired-listing-switches "-alh")

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'solarized-dark)

(require 'evil)
(evil-mode 1)
(evil-set-initial-state 'term-mode 'emacs) ; turn off in terminal mode
(setq evil-want-C-u-scroll t) ; turn off C-u emacs binding

;; disable in certain modes
(dolist (mode '(
		eww-mode
		dired-mode
		)
	      )
  (add-to-list 'evil-emacs-state-modes mode))

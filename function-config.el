;; server setup
(defun start-emacs-server-if-not-running ()
  
    (if (and (fboundp 'server-running-p)
	     (not (server-running-p)))
	
	(message "Server not running - starting the server.")
      	(server-start)
      
      (message "Server already running")
      )
  )
